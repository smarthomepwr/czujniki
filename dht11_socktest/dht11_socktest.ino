#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <DHTesp.h>
#include <Ticker.h>
#include <ArduinoOTA.h>
#include <ESP8266mDNS.h>

Ticker sensorRegister;

#define DHT11_PIN 5

DHTesp dht;

unsigned long previousMillis = 0;
const long interval = 2000;

// Wifi Stuff
const char* ssid = "SmartHome";
const char* password = "SmartHome123";


WiFiUDP ntpUDP;
char incomingPacket[255];
char replyPacket[] = "accepted";
IPAddress ipMulti(10, 0, 0, 255);
unsigned short dataPort = 12345;
bool accepted=0;


void setup() 
{
  pinMode(4, INPUT);
  pinMode(2, OUTPUT);
  Serial.begin(115200);
  Serial.println();

  Serial.printf("Connecting to %s ", ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(250);
    Serial.print(".");
    digitalWrite(2, LOW);
    delay(250);
    digitalWrite(2, HIGH);
  }
  Serial.println(" connected");
  ntpUDP.begin(dataPort);
  dht.setup(DHT11_PIN, DHTesp::DHT11);
  sensorRegister.attach(2, registerSensor);
  ArduinoOTA.onStart([]() 
  {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";
  });
  ArduinoOTA.onEnd([]()
  {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) 
  {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) 
  {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
}

void loop() 
{
  while(accepted==0)
  {
    receiveReply();
    ArduinoOTA.handle();
  }
  if(accepted==1)
  {
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis > interval) 
    {
      previousMillis = currentMillis;
      getTimeandTemp();
    }
  }
  ArduinoOTA.handle();
 
}


void getTimeandTemp() {
  
  float humidity = dht.getHumidity();
  float temperature = dht.getTemperature();
  StaticJsonBuffer<200> sensorBuffer;
  JsonObject& data = sensorBuffer.createObject();
  data["packet_type"] = "data";
  data["sensor_type"] = "temperature";
  data["name"] = "temp_hum_sensor";
  data["temperature"] = temperature;
  data["humidity"] = humidity;
 
 
  ntpUDP.beginPacket(ipMulti, dataPort);
  data.printTo(ntpUDP);
  ntpUDP.println();
  ntpUDP.endPacket();
//  ESP.deepSleep(2e6, WAKE_NO_RFCAL);
}

void registerSensor() 
{
  if (accepted==0)
  {
    digitalWrite(2, LOW);
    //send info packet
    String ipAddress = WiFi.localIP().toString().c_str();
    StaticJsonBuffer<200> commandBuffer;
    JsonObject& command = commandBuffer.createObject();
    command["packet_type"] = "control";
    command["sensor_type"] = "temperature";
    command["name"] = "temp_hum_sensor";
    command["IP"] = ipAddress;
    command["SSID"] = ssid;

    ntpUDP.beginPacket(ipMulti, dataPort);
    command.printTo(ntpUDP);
    ntpUDP.println();
    ntpUDP.endPacket();
    //receive control packet
    digitalWrite(2, HIGH);
  }
}

void receiveReply()
{
    
  int packetSize = ntpUDP.parsePacket();
  if (packetSize)
{
  Serial.printf("Received %d bytes from %s, port %d\n", packetSize, ntpUDP.remoteIP().toString().c_str(), ntpUDP.remotePort());
  int len = ntpUDP.read(incomingPacket, 255);
  if (len > 0)
  {
    incomingPacket[len] = 0;
  }
  Serial.printf("UDP packet contents: %s\n", incomingPacket);
  String received = incomingPacket;
  if(received=="accepted") 
  {
    accepted=1;
    blinkAccept();
  }
}
  
  //Serial.println(accepted);
}

void blinkAccept()
{
  for(int i=0 ; i<10 ; i++)
  {
    digitalWrite(2, LOW);
    delay(50);
    digitalWrite(2, HIGH);
    delay(50);
  }
}

